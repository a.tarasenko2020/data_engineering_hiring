# Data Engineering hiring

This is an intro data engineering task for candidates

## Instructions

### Task

Your task is to scrape http://books.toscrape.com/ website

We expect from you `books.csv` file with columns [`title`, `description`, `price`, `UPC`, `rating`] and the python scripts


### Tips

* use scrapy to extract the data
* use proxies when you perform request


# Submission 

Write your code in the develop branch and submit a MergeRequest to @mfeldman to us once you're done. In case of questions, add a ticket to your GitLab Issue board and assign @mfeldman
